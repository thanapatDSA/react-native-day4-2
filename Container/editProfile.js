
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import styles from '../style'

class editProfile extends Component {
    goToProfile = () => {
        this.props.history.push('./profile')
    }
    render() {
        const { profile } = this.props
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{ flex: 1, width: 50, height: 50, backgroundColor: 'powderblue' }} >
                        <Button style={{ height: 50 }} onPress={() => { this.goToProfile() }}>Back</Button>
                    </View>
                    <View style={{ flex: 2, width: 50, height: 50, backgroundColor: 'powderblue', alignItems: 'center' }} >
                        <Text style={{ fontSize: 20, padding: 12 }}>EditProfile</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <ScrollView>
                        <InputItem
                            clear
                            onChange={() => { }}
                            placeholder="First name"
                            defaultValue= {profile[0].firstname}
                        >First_Name</InputItem>
                        <InputItem
                            clear
                            onChange={() => { }}
                            placeholder="Last name"
                            defaultValue= {profile[0].lastname}
                        >Last_Name</InputItem>
                    </ScrollView>

                </View>
                <View style={styles.footer}>
                    <View style={{ height: 50, backgroundColor: 'powderblue', flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Button onPress={() => { }}>SAVE</Button>
                        </View>
                    </View>
                </View>
            </View>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (username, firstname, lastname) => {
            dispatch({
                type: 'ADD_ITEM',
                username: username,
                firstname: firstname,
                lastname: lastname
            })
        }
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(editProfile)