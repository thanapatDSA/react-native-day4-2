export default (state = [], action) => {
  switch (action.type) {
    case 'ADD_PROFILE':
      return [...state, {
        username: action.username,
        password: action.password,
        firstname: action.firstname,
        lastname: action.lastname
      }]
    case 'REMOVE_PROFILE':
      return state.filter((item, index) => index !== action.index)
    default:
      return state
  }
}