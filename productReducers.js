
export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_ITEM':
            return [...state, {
                pic: action.pic,
                name: action.name
            }]
        case 'REMOVE_ITEM':
            return state.filter((item, index) => index !== action.index)
        default:
            return state
    }
}

